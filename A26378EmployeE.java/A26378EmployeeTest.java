
public class Employee {
	private String firstName;
	private String lastName;
	private double salary;
	
	public Employee(String firstName, String lastName, double salary) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		if(salary > 0)
			this.salary = salary;
		else
			this.salary = 0.0;
	}
	
	public Employee() {
		this.firstName = null;
		this.lastName = null;
		this.salary = 0.0;
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		if(salary > 0)
			this.salary = salary;
		else
			this.salary = 0.0;
	}
	
}
----
*\A26378EmployeeTest.java\*

public class EmployeeTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Employee emp1 = new Employee("anh", "tuan", 10);
		
		System.out.println("luong nhan vien emp1 la: " + emp1.getSalary() * 12);
		
		Employee emp2 = new Employee("hoa", "nguyen", 9);
		System.out.println("luong nhan vien emp2 la: " + emp2.getSalary() * 12);
		
		emp1.setSalary(emp1.getSalary() * 1.1);
		
		System.out.println("sau khi tang luong 10%, luong  emp1 la: " + emp1.getSalary() * 12);
		 
		emp2.setSalary(emp2.getSalary() * 1.1);
		System.out.println("sau khi tang luong 10%, luong emp2 la: " + emp2.getSalary() * 12);
		
	}

}

